<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTakingSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taking_subjects', function (Blueprint $table) {
            
            $table->bigIncrements('id');

            $table->string('student_id');
            $table->foreign('student_id')->references('Userid')->on('users');


            $table->string('subjectcode');
            $table->foreign('subjectcode')->references('code')->on('subjects');;
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taking_subjects');
    }
}
