<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('stud_id');
            $table->string('stud_name');
            $table->string('stud_department');
            $table->string('adddrop');

            $table->bigInteger('subjectcode')->unsigned()->index()->nullable();
            $table->foreign('subjectcode')->references('code')->on('subjects');

            $table->bigInteger('subjectname')->unsigned()->index()->nullable();
            $table->foreign('subjectname')->references('name')->on('subjects');

            $table->string('reason');
            $table->string('propose');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application');
    }
}
