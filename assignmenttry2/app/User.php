<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','Userid', 'password', 'role','department'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function subject()
    {
        return $this->hasMany(Subject::class);
    }

    public function subject2()
    {
        return $this->belongsToMany('App\Subject','subject_user');
        
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department');
    }
    
    public function application()
    {
        return $this->belongsToMany('App\Application','applications');
        
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
