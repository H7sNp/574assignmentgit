<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    
    
    public function user()
    {
        return $this->hasMany('App\User');
    }
    public function subject()
    {
        return $this->hasMany('App\Subject');
    }
}
