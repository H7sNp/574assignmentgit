<?php

namespace App\Http\Controllers;
use App\User;
use App\Subject;
use App\Department;
use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HodController extends Controller
{
    public function student()
    {

        //$hod=User::where('role','Student')->get();
        $hod=User::all();
        return $hod;

        return view('hodstudents',compact('hod'));
      
    }

    public function profile()
    {
        $hod= User::with('department')->get();
        

        return view('hodprofile',compact('hod'));
        
    }

    public function application()
    {
        //$apply=Application::where('department',Auth::User()->department)->get();
        $apply=Application::with('user')->get();
        return $apply;
        return view('hodapplications');
    }

    public function editmail(Request $request)
    {
        
        $owner= \App\User::find(Auth::id());
        
        //$owner = User::where('Userid',Auth::User()->Userid)->get();
        $owner->email= $request->get('email');
        $owner->save();

        if(Auth::user()->role=='Student')
        {
            return redirect('/studentprofile')->with('success','Email Updated :D');
        }
        else
        {
            return redirect('/hodprofile')->with('success','Email Updated :D');
        }
        
    }

    public function apply()
    {
        return view('hodapplications');
    }

    
}
