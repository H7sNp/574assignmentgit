<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable =[''];

    public function user()
    {
        return $this->belongsToMany('App\User');
    }

    public function dept()
    {
        
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function application()
    {
        return $this->belongsToMany('App\Application');
        
    }

}