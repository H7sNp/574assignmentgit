<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Application extends Model

{
    public function user()
    {
        return $this->belongsToMany('App\User');
        
    }
    public function subject()
    {
        return $this->hasMany('App\Subject');
    }
}
