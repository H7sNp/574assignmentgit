<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/testhod', function () {
    return view('layouts.homepagehod');
});

Route::get('/teststud', function () {
    return view('layouts.homepage');
});



/////////////////////////////////////////////////////////////////////////////
Route::get('/studentprofile', 'StudentController@index1');

Route::get('/studentsubject', 'StudentController@index2');

Route::get('/studentadddrop', 'StudentController@index3');

Route::post('/editmailstud','HodController@editmail');

Route::post('/addordrop','StudentController@adddrop');

////////////////////////////////////////////////////////////////////////////

Route::get('/allstudents','StudentController@index');

Route::get('/alldepartment','DepartmentController@index');

Route::get('/allsubject','SubjectController@index');






///////////////////////////////////////////////////////////////////////////////////////////////
Route::get('/hodprofile','HodController@profile');

Route::get('/hodstudent','HodController@student');

Route::get('/hodapp','HodController@application');

Route::post('/editmailhod','HodController@editmail');

///////////////////////////////////////////////////////////////////////////////////////////////

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

///////////////////////////////////////////////////////////////////////////////////////////////

Route::get('sendemail', function () {
    $data = array(
    'name' => "Hazli",
    );
    Mail::send('emailstest',$data, function($message){
    $message->from('lot1866a@gmail.com', 'test email');
    $message->to('muhdhariz8uniten@gmail.com')->subject('Testing sending email');
    });
    return "Your email has been sent!";
    });

///////////////////////////////////////////////////////////////////////////////////////////////
Route::post('/apply','ApplicationController@store');

///////////////////////////////////////////////////////////////////////////////////////////////