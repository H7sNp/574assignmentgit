@extends('layouts.homepage')

@section('content')
<div class="card-header" style="width: 1000px;">
    <strong class="card-title">Subjects offered for 
    {{Auth::User()->Department['depname']}}
  </strong>
</div>
<table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Subject Code</th>
            <th scope="col">Subject Name</th>
            
            
        </tr>
        </thead>
        <tbody>
        @if ($i==0)
            <tr>
                <td colspan="5"> No Data </td>
            </tr>
        @else   
            @foreach($sub as $subs)
                <tr>
                    <th scope="row"> {{$i++}} </th>
                    <td>{{$subs->code}}</td>
                    <td>
                        
                        {{$subs->name}}
                        <br>
                    </td>

                    
                    
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>


                    <div style="float: left;" class="col-lg-18">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Subjects Taking</strong>
                            </div>
                            <div class="card-body">
                                <table  class="table table-dark">
                                    <thead>
                                        <tr>
                                            <th scope="col">Subject Code</th>
                                            <th scope="col">Subject Name</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if ($i2==0)
                                        <tr>
                                            <td colspan="3"> No Data </td>
                                        </tr>
                                    @else   
                                         @foreach($stud2 as $stud2s)
                                        <tr>
                                            
                                            @foreach($stud2s->subject2 as $lol)
                                            <tr>
                                                <td>{{$lol->code}}</td>
                                            
                                                <td>{{$lol->name}}</td>
                                            </tr>
                                            @endforeach
                                           
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>
@endsection