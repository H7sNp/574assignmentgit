@extends('layouts.homepage')

@section('content')

<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 300px;
  margin: auto;
  text-align: center;
  font-family: arial;
}

.title {
  color: grey;
  font-size: 28px;
}

button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 18px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 28px;
}
.button1 {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 11px;
  color: white;
  background-color: blue;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 16px;
}

.button2 {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 11px;
  color: white;
  background-color: red;
  text-align: float:right ;
  cursor: pointer;
  width: 20%;
  font-size: 16px;
}

a {
  text-decoration: none;
  font-size: 32px;
  color: black;
}

button:hover, a:hover {
  opacity: 0.7;
}
</style>
</head>
<body>

<h2 style="text-align:center">User Profile Card</h2>

<div class="card" style="width: 1000px;">
  <img src="images/sadcat.jpg" alt="userpic" style="width:100%">
  <h1></h1>
  <p class="title">{{Auth::User()->name}}</p>
  <p>{{Auth::User()->Userid}}</p>
  <p>{{Auth::User()->email}}</p>
  <p>{{Auth::User()->Department['depname']}}</p>
  
  <p><button data-toggle="modal" data-target="#exampleModal" >Update E-Mail</button></p>
</div>

</body>
</html>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">EDIT EMAIL</h5>
        
      </div>
        <!--MODAL CONTENT HERE!-->
      <div class="modal-body">
      <form action="/editmailstud" method="POST">
      @csrf


  
  <div class="form-group">
    <label for="exampleInputPassword1">Email</label>
    <input name="email" type="text" class="form-control" id="exampleInputPassword1" placeholder="New Email" value="{{Auth::User()->email}}">
  </div>
  
  <button style="align:center" type="submit" class="button1 btn btn-primary">Submit</button>
</form>
      </div>
        
      <div style="align:center" class="modal-footer">
        <button type="button" class="button2 btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      <!--MODAL ENDS HERE!-->

@endsection