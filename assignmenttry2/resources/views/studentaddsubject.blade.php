@extends('layouts.homepage')

@section('content')
            
                                                <div class="card" style="width: 1000px;">
                                                    <div class="card-header">
                                                        <strong>Add-Drop Subject</strong>                                                                     
                                                    </div>
                                                    <div class="card-body card-block">


                                                        <form action="/apply" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                                            @csrf
                                                           
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label class=" form-control-label">Student Id</label></div>
                                                                <div class="col-12 col-md-9">
                                                                    <p class="form-control-static">{{Auth::user()->Userid}}</p><input value="{{Auth::User()->id}}"name="userid" type="hidden">
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label class=" form-control-label">Name</label></div>
                                                                <div class="col-12 col-md-9">
                                                                    <p class="form-control-static">{{Auth::user()->name}}</p><input value="{{Auth::User()->name}}" name="name" type="hidden">
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label class=" form-control-label">Department</label></div>
                                                                <div class="col-12 col-md-9">
                                                                    <p class="form-control-static">{{Auth::User()->Department['depname']}}</p><input value="{{Auth::User()->Department['id']}}" name="depid" type="hidden">
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                    <div class="col col-md-3"><label for="select" class=" form-control-label">Select Subject</label></div>
                                                                    <div class="col-12 col-md-9">
                                                                        <select name="subjectid" id="select" class="form-control">
                                                                                
                                                                            <option value="">Please select</option>
                                                                            @foreach($sub as $subs)
                                                                            <option value="{{$subs->id}}">{{$subs->code}} {{$subs->name}}</option>
                                                                            @endforeach
                                                                            
                                                                        </select>
                                                                    </div>
                                                                
                                                            </div>

                                                            <div class="row form-group">
                                                                    <div class="col col-md-3"><label class=" form-control-label">Add or Drop</label></div>
                                                                    <div class="col col-md-9">
                                                                        <div class="form-check">
                                                                            <div class="radio">
                                                                                <label for="radio1" class="form-check-label ">
                                                                                    <input type="radio" id="radio1" name="adddrop" value="Add" class="form-check-input">Add
                                                                                </label>
                                                                            </div>
                                                                            <div class="radio">
                                                                                <label for="radio2" class="form-check-label ">
                                                                                    <input type="radio" id="radio2" name="adddrop" value="Drop" class="form-check-input">Drop
                                                                                </label>
                                                                            </div>
                                                                           
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                           
                                                           
                                                            
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Reason</label></div>
                                                                <div class="col-12 col-md-9"><textarea name="reason" id="textarea-input" rows="9" placeholder="Content..." class="form-control"></textarea></div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Proposed Solution</label></div>
                                                                <div class="col-12 col-md-9"><textarea name="proposal" id="textarea-input" rows="9" placeholder="Content..." class="form-control"></textarea></div>
                                                            </div>
                                                                
                                                                <div class="card-footer">
                                                                    <button type="submit" class="btn btn-primary btn-sm">
                                                                        <i class="fa fa-dot-circle-o"></i> Submit
                                                                    </button>
                                                                    <button type="reset" class="btn btn-danger btn-sm">
                                                                        <i class="fa fa-ban"></i> Reset
                                                                    </button>
                                                                </div>
                                                                
                                                        </form>

                                                    </div>
                                                    
                                                </div>
                                               
                                            </div>
            


@endsection