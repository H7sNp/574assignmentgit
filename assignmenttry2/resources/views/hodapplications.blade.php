@extends('layouts.homepagehod')

@section('content')

<div class="card-header" style="width: 1000px;">
    <strong class="card-title">Add-Drop Applications for
    {{Auth::User()->Department['depname']}}
  </strong>
</div>
<table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Student ID</th>
            <th scope="col">Student Name</th>
            <th scope="col">Subject Code</th>
            <th scope="col">Subject Name</th>
            <th scope="col">Add/Drop</th>
            <th scope="col">Reason</th>
            <th scope="col">Proposal</th>
            
            
        </tr>
        </thead>
        <tbody>
        @if ($i==0)
            <tr>
                <td colspan="9"> No Data </td>
            </tr>
        @else   
            @foreach($sub as $subs)
                <tr>
                    <th scope="row"> {{$i++}} </th>
                    <td>{{$subs->code}}</td>
                    <td>{{$subs->name}}</td>

                    
                    
                </tr>
            @endforeach
        @endif
        
        </tbody>
    </table>
@endsection     